/**
 * 
 */
package com.idyria.osi.tea.classpath;

/**
 * Used to detect detection of class file
 * @author Richnou
 *
 */
public interface ClassFoundListener {

	
	public void classFileFound(String className);
	
}
