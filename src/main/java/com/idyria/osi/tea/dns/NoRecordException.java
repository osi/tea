/**
 * 
 */
package com.idyria.osi.tea.dns;

/**
 * @author Richnou
 *
 */
public class NoRecordException extends Exception {

	/**
	 * 
	 */
	public NoRecordException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public NoRecordException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public NoRecordException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public NoRecordException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
