/**
 * 
 */
package com.idyria.osi.tea.i2n;

/**
 * @author rtek
 *
 */
public class TeaI2nUniverseNotFound extends Exception {

	/**
	 * 
	 */
	public TeaI2nUniverseNotFound() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public TeaI2nUniverseNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public TeaI2nUniverseNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public TeaI2nUniverseNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
